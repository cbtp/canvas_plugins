package com.canvas.plugin.hanime1


import com.canvas.plugin.data.mapping.*
import com.canvas.plugin.data.repository.IComicsRepository
import com.canvas.plugin.data.result.KResult
import okhttp3.*
import org.json.JSONObject

class KKMHRepository(private val httpClient: OkHttpClient) : IComicsRepository {
    private var comicsTags: MutableList<ComicsTag> = mutableListOf()
    private val baseUrl = "https://api.kkmh.com"
    override suspend fun fetchComicsList(pageInt: Int): List<Comics> {
        val url = "$baseUrl/v1/topic_new/lists/get_by_tag?tag=0&since=${pageInt * 10}"
        val request = Request.Builder()
            .url(url)
            .get()
            .build()
        val response = httpClient.newCall(request).execute().body
        val list = response?.let {
            parserComicsList(it.string())
        } ?: listOf()

        return list
    }

    private fun parserComicsList(response: String): List<Comics> {
        val dataJsonObject = JSONObject(response).getJSONObject("data")
        val tagsArray = dataJsonObject.getJSONArray("tags")
        comicsTags.clear()
        for (i in 0 until tagsArray.length()) {
            val id = tagsArray.getJSONObject(i).getInt("tag_id")
            val title = tagsArray.getJSONObject(i).getString("title")
            comicsTags.add(ComicsTag(id.toString(), title))
        }
        val topicsArray = dataJsonObject.getJSONArray("topics")
        val list = mutableListOf<Comics>()
        for (i in 0 until topicsArray.length()) {
            val id = topicsArray.getJSONObject(i).getInt("id")
            val title = topicsArray.getJSONObject(i).getString("title")
            val desc = topicsArray.getJSONObject(i).getString("description")
            val cover = topicsArray.getJSONObject(i).getString("cover_image_url")
            list.add(Comics(id.toString(), title, cover, desc, "$baseUrl/v1/topics/$id"))
        }
        return list
    }


    override suspend fun fetchComicsDetail(comics: Comics): KResult<ComicsDetail> {
        val list = loadMoreChapters(comics.href)

        return KResult.Success(ComicsDetail(list[list.lastIndex].updateTime, list))
    }

    private fun loadMoreChapters(href: String): List<ComicsChapter> {
        val request = Request.Builder().url(href).get().build()
        val response = httpClient.newCall(request)
            .execute().body?.string()

        val chapterJsonArray = JSONObject(response).getJSONObject("data").getJSONArray("comics")
        val list = mutableListOf<ComicsChapter>()
        for (i in 0 until chapterJsonArray.length()) {
            val chapterObject = chapterJsonArray.getJSONObject(i)
            val cover = chapterObject.getString("cover_image_url")
            val title = chapterObject.getString("title")
            val id = chapterObject.getInt("id").toString()
            val pageCount = chapterObject.getInt("image_count")
            val updateTime = chapterObject.getLong("updated_at").toString()
            val canView = chapterObject.getBoolean("can_view")
            if (!canView) {
                continue
            }
            list.add(ComicsChapter(id, cover, title, "$baseUrl/v2/comic/$id", pageCount, updateTime))
        }
        return list
    }

    override suspend fun fetchComicsBookList(
        comics: Comics,
        chapter: ComicsChapter
    ): KResult<ComicsBook> {
        val request = Request.Builder().url(chapter.href)
            .build()
        val response = httpClient.newCall(request)
            .execute().body?.string()
        val imageJsonArray = JSONObject(response).getJSONObject("data").getJSONArray("images")
        val list = mutableListOf<String>()
        for (i in 0 until imageJsonArray.length()) {
            val url = imageJsonArray[i]
            list.add(url.toString())
        }
        return KResult.Success(ComicsBook(list))
    }

    override suspend fun fetchFilterList(): KResult<List<String>> {
        TODO("Not yet implemented")
    }

    override suspend fun filter(filter: String): KResult<Unit> {
        TODO("Not yet implemented")
    }

    override suspend fun search(search: String): KResult<List<Comics>> {
        TODO("Not yet implemented")
    }


}