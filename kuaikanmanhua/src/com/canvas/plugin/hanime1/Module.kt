package com.canvas.plugin.hanime1

import com.canvas.plugin.data.repository.IComicsRepository
import org.koin.core.qualifier.named
import org.koin.dsl.module

val KKMHModule = module {
    single<IComicsRepository>(named("kuaiKanManHua"), override = true) {
        KKMHRepository(get())
    }
}