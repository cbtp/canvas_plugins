package com.canvas.plugin.hanime1

import com.canvas.plugin.core.IKoinRegister
import org.koin.core.context.loadKoinModules
import org.koin.core.context.unloadKoinModules

class KoinRegister : IKoinRegister {
    override fun register() {
        loadKoinModules(KKMHModule)
    }

    override fun unregister() {
        unloadKoinModules(KKMHModule)
    }
}