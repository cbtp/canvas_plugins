package com.canvas.plugin.hanime1

import com.canvas.plugin.core.IPluginProperty
import com.canvas.plugin.core.PluginType

class KKMHProperty :IPluginProperty {
    override val qualifierName: String
        get() = "kuaiKanManHua"
    override val pluginType: PluginType
        get() = PluginType.COMICS

}