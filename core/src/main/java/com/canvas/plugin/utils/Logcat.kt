package com.canvas.plugin.utils

import android.os.Build
import android.util.Log
import java.util.regex.Pattern

/**
 * Created by kwocin
 * des:打印日志类
 * on 2020/12/2.
 */
object Logcat {
    private var verbose = false
    private var debug = false
    private var info = false
    private var warn = false
    private var erro = false
    fun config(all: Boolean = false) {
        this.verbose = all
        this.debug = all
        this.info = all
        this.warn = all
        this.erro = all
    }

    fun config(
        verbose: Boolean = false, debug: Boolean = false,
        info: Boolean = false, warn: Boolean = false,
        erro: Boolean = false,
    ) {
        this.verbose = verbose
        this.debug = debug
        this.info = info
        this.warn = warn
        this.erro = erro

    }

    fun v(tag: String = getTag(), message: String) {
        if (verbose) Log.v(tag, message)
    }

    fun d(tag: String = getTag(), message: String) {
        if (debug) Log.d(tag, message)
    }

    fun i(tag: String = getTag(), message: String) {
        if (info) Log.i(tag, message)
    }

    fun w(tag: String = getTag(), message: String) {
        if (warn) Log.w(tag, message)
    }

    fun e(tag: String = getTag(), message: String) {
        if (erro) Log.e(tag, message)
    }

    private fun getTag(): String {
        val stackTrace = Throwable().stackTrace
        if (stackTrace.size < 3) {
            throw IllegalStateException("Don't have enough elements. Are you using proguard?")
        }
        var tag = stackTrace[2].className
        val matcher = Pattern.compile("(\\$\\d+)+$").matcher(tag)
        if (matcher.find()) {
            tag = matcher.replaceAll("")
        }
        tag = tag.substring(tag.lastIndexOf(".") + 1)
        return if (tag.length < 24 || Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            tag
        } else {
            tag.substring(0, 23)
        }
    }
}