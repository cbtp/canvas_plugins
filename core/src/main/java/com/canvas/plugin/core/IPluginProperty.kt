package com.canvas.plugin.core



interface IPluginProperty {
    val qualifierName:String
    val pluginType:PluginType
}