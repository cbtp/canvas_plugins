package com.canvas.plugin.core

interface IKoinRegister {
    fun register()
    fun unregister()
}