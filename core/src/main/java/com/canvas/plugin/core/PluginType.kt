package com.canvas.plugin.core

enum class PluginType {
    PHOTO, COMICS, VIDEO
}

