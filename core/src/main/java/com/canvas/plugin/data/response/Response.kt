package com.canvas.plugin.data.response

open class Response<T>(
    val stateCode:Int=0,
    val message:String="",
    val data:T?=null
)

