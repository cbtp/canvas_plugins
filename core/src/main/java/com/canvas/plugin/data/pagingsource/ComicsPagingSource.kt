package com.canvas.plugin.data.pagingsource

import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.canvas.plugin.data.mapping.Comics
import com.canvas.plugin.data.mapping.Photo
import com.canvas.plugin.data.repository.IComicsRepository
import com.canvas.plugin.data.repository.IPhotoRepository

class ComicsPagingSource(val repository: IComicsRepository) : PagingSource<Int, Comics>() {
    override fun getRefreshKey(state: PagingState<Int, Comics>): Int? {
        return state.anchorPosition?.let { anchorPosition ->
            val anchorPage = state.closestPageToPosition(anchorPosition)
            anchorPage?.prevKey?.plus(1) ?: anchorPage?.nextKey?.minus(1)
        }
    }

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, Comics> {
        try {
            // Start refresh at page 1 if undefined.
            val nextPageNumber = params.key ?: 0
            val response = repository.fetchComicsList(nextPageNumber)
            return LoadResult.Page(
                data = response,
                prevKey = null, // Only paging forward.
                nextKey = nextPageNumber + 1,
            )
        } catch (e: Exception) {
            e.printStackTrace()
            return LoadResult.Error(e)
        }
    }
}