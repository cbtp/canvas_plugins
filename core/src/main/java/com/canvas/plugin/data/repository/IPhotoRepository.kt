package com.canvas.plugin.data.repository

import androidx.paging.PagingData
import com.canvas.plugin.data.mapping.Photo
import com.canvas.plugin.data.mapping.PhotoCategory
import com.canvas.plugin.data.result.KResult


interface IPhotoRepository {
<<<<<<< HEAD
    suspend fun fetchPhotoList(page: Int): KResult<List<Photo>>
=======
    suspend fun fetchPhotoList(page: Int): List<Photo>
>>>>>>> 620f5dbaaf06e9b00dabd03db286d10adbff7492
    suspend fun fetchCategoryList(): KResult<List<PhotoCategory>>
    suspend fun fetchCategoryPhotoList(category: PhotoCategory, page: Int): KResult<List<Photo>>
    suspend fun fetchSearchPhotoList(search: String, page: Int): KResult<List<Photo>>
}