package com.canvas.plugin.data.mapping

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
open class Comics(
    var comicsId:String,
    var name: String = "",
    var cover: String = "",
    var detail: String = "",
    var href: String = ""
) : Parcelable

@Parcelize
open class ComicsDetail(val updateTime: String, var chapters: List<ComicsChapter>) : Parcelable

@Parcelize
open class ComicsChapter(
    var chapterId:String,
    var cover: String? = null,
    var name: String,
    var href: String,
    val pagesCount: Int,
    val updateTime: String = ""
) :
    Parcelable

@Parcelize
open class ComicsBook(var pages: List<String>) : Parcelable
@Parcelize
open class ComicsTag(var tagId:String,val title:String): Parcelable