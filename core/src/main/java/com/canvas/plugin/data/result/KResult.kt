package com.canvas.plugin.data.result

sealed class KResult<T> {
    var isSuccess: Boolean = false
    abstract fun get(): T?
    abstract fun getThrowable(): Throwable?
    data class Success<T>(private val result: T) : KResult<T>() {
        init {
            isSuccess = true
        }

        override fun get(): T = result

        override fun getThrowable(): Throwable? {
            return null
        }
    }


    data class Failure<T>(private val throwable: Throwable, private val result: T? = null) : KResult<T>() {
        init {
            isSuccess = false
        }

        override fun get(): T? {
            return result
        }


        override fun getThrowable(): Throwable {
            return throwable
        }

    }
}

fun <T> KResult<T>.onSuccess(block: KResult<T>.(result: T) -> Unit): KResult<T> {
    if (this.isSuccess) {
        block(get()!!)
    }
    return this
}

fun <T> KResult<T>.onFailure(block: KResult<T>.(result: T?, throwable: Throwable) -> Unit): KResult<T> {
    if (!this.isSuccess) {
        block(get(), getThrowable()!!)
    }
    return this
}