package com.canvas.plugin.data.repository

import com.canvas.plugin.data.mapping.*
import com.canvas.plugin.data.result.KResult


interface IComicsRepository {
    suspend fun fetchComicsList(page: Int):List<Comics>
    suspend fun fetchComicsDetail(comics: Comics): KResult<ComicsDetail>
    suspend fun fetchComicsBookList(comics: Comics, chapter: ComicsChapter): KResult<ComicsBook>
    suspend fun fetchFilterList(): KResult<List<String>>
    suspend fun filter(filter: String): KResult<Unit>
    suspend fun search(search: String): KResult<List<Comics>>
}