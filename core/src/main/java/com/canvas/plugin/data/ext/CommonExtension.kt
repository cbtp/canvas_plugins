package com.canvas.plugin.data.ext

import android.content.res.Resources
import android.util.TypedValue
import java.math.RoundingMode
import java.text.DecimalFormat
import kotlin.random.Random

val random: Random by lazy{
    Random(System.currentTimeMillis())
}

fun randomColor():Int{
    return random.nextInt(0x55000000, 0x55ffffff)
}
val Float.dp: Float
    get() {
        return TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_DIP,
            this,
            Resources.getSystem().displayMetrics
        )
    }

val Int.dp: Float
    get() {
        return toFloat().dp
    }
val Float.dpti: Int
    get() {
        return dp.toInt()
    }

val Int.dpti: Int
    get() {
        return dp.toInt()
    }
/**
 * 保留 N 位小数
 */
fun Float.digits(n: Int = 2): String {
    val stringBuilder = StringBuilder("0.")
    for (i in 0 until n) {
        stringBuilder.append("#")
    }
    val format = DecimalFormat(stringBuilder.toString())
    format.roundingMode = RoundingMode.HALF_UP
    return format.format(this)
}