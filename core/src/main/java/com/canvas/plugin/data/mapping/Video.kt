package com.canvas.plugin.data.mapping

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

/**
 * Created by alloyaxe
 * on 2021/6/13
 */

@Parcelize
open class Video(
    var videoId:String,
    var name: String = "",
    var cover: String = "",
    var detail: String = "",
    var href: String = ""
) : Parcelable

@Parcelize
open class VideoDetail(val updateTime: String, var episodes: List<VideoEpisode>) : Parcelable

@Parcelize
open class VideoEpisode (
    var episodeId:String,
    var cover: String? = null,
    var name: String,
    var href: String,
    val updateTime: String = ""
) :
    Parcelable

@Parcelize
open class VideoTag(var tagId:String,val title:String): Parcelable