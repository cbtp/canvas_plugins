package com.canvas.plugin.data.mapping

import android.os.Parcelable
import androidx.annotation.ColorInt
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Photo(
    val path:String,
    @ColorInt
    val color:Int

) : Parcelable

@Parcelize
data class PhotoCategory(val id: String, val name: String) : Parcelable
