package com.canvas.plugin.data.repository

import com.canvas.plugin.data.mapping.*
import com.canvas.plugin.data.result.KResult


interface IVideoRepository {
    suspend fun fetchVideoList(page: Int): List<Video>
    suspend fun fetchVideoDetail(video: Video): KResult<VideoDetail>
    suspend fun fetchVideoUrls(comics: Video, episode: VideoEpisode): KResult<String>
    suspend fun fetchFilterList(): KResult<List<String>>
    suspend fun filter(filter: String): KResult<Unit>
    suspend fun search(search: String): KResult<List<Video>>
}