package com.canvas.plugin.hanime1


import com.canvas.plugin.data.mapping.*
import com.canvas.plugin.data.repository.IComicsRepository
import com.canvas.plugin.data.repository.IVideoRepository
import com.canvas.plugin.data.result.KResult
import com.canvas.plugin.utils.Logcat
import okhttp3.*
import org.json.JSONObject
import org.jsoup.Jsoup

class Hanime1Repository(private val httpClient: OkHttpClient) : IVideoRepository {
    override suspend fun fetchVideoList(page: Int): KResult<List<Video>> {
        val request = Request.Builder()
            .url("https://hanime1.me/search?genre=H%E5%8B%95%E6%BC%AB&page=${page + 1}").build()
        val response = httpClient.newCall(request).execute().body?.string() ?: ""
        val doc = Jsoup.parse(response)
        val videoDivs = doc.getElementsByClass("home-rows-videos-wrapper")[0].getElementsByTag("a")
        val list = videoDivs.map { div ->
            val href = div.attr("href")

            val cover = div.getElementsByTag("img").attr("src")
            val title = div.getElementsByClass("home-rows-videos-title").text()
            Video("", title, cover, "", href)
        }
        return KResult.Success(list)    }

    override suspend fun fetchVideoDetail(video: Video): KResult<VideoDetail> {
        val request = Request.Builder().url(video.href).build()
        val response = httpClient.newCall(request).execute().body?.string() ?: ""
        val doc = Jsoup.parse(response)
        val json = doc.getElementsByTag("script").text()
        Logcat.i(message = "json $json")
        Logcat.i(message = "doc $doc")
        val videoUrl = JSONObject(json).getString("contentUrl")
//        doc.getElementById("player").attr("src")
        val updateTime =
            doc.getElementsByClass("video-show-panel-width").first().getElementsByTag("p").text()
        val name = doc.getElementById("shareBtn-title").text()
        val videoDetail = VideoDetail(
            updateTime,
            listOf(VideoEpisode("", "", name, videoUrl, updateTime))
        )
        Logcat.i(message = "hanime1")
        return KResult.Success(videoDetail)
    }

    override suspend fun fetchVideoUrls(comics: Video, episode: VideoEpisode): KResult<String> {
        TODO("Not yet implemented")
    }

    override suspend fun fetchFilterList(): KResult<List<String>> {
        TODO("Not yet implemented")
    }

    override suspend fun filter(filter: String): KResult<Unit> {
        TODO("Not yet implemented")
    }

    override suspend fun search(search: String): KResult<List<Video>> {
        TODO("Not yet implemented")
    }


}