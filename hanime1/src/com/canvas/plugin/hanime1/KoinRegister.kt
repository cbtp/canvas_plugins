package com.canvas.plugin.hanime1

import com.canvas.plugin.core.IKoinRegister
import org.koin.core.context.loadKoinModules
import org.koin.core.context.unloadKoinModules

class KoinRegister : IKoinRegister {
    override fun register() {
        loadKoinModules(Hanime1Module)
    }

    override fun unregister() {
        unloadKoinModules(Hanime1Module)
    }
}