package com.canvas.plugin.hanime1

import com.canvas.plugin.core.IPluginProperty
import com.canvas.plugin.core.PluginType

class Hanime1Property :IPluginProperty {
    override val qualifierName: String
        get() = "hanime1"
    override val pluginType: PluginType
        get() = PluginType.VIDEO

}