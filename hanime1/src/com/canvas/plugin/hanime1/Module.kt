package com.canvas.plugin.hanime1

import com.canvas.plugin.data.repository.IComicsRepository
import com.canvas.plugin.data.repository.IVideoRepository
import org.koin.core.qualifier.named
import org.koin.dsl.module

val Hanime1Module = module {
    single<IVideoRepository>(named("hanime1"),override = true) {
        Hanime1Repository(get())
    }
}