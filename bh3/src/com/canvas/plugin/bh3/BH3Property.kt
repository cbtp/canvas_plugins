package com.canvas.plugin.bh3

import com.canvas.plugin.core.IPluginProperty
import com.canvas.plugin.core.PluginType

class BH3Property :IPluginProperty {
    override val qualifierName: String
        get() = "BH3"
    override val pluginType: PluginType
        get() = PluginType.COMICS

}