package com.canvas.plugin.bh3


import com.canvas.plugin.bh3.data.ComicsChapterJson
import com.canvas.plugin.data.mapping.Comics
import com.canvas.plugin.data.mapping.ComicsBook
import com.canvas.plugin.data.mapping.ComicsChapter
import com.canvas.plugin.data.mapping.ComicsDetail
import com.canvas.plugin.data.repository.IComicsRepository
import com.canvas.plugin.data.result.KResult
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.coroutines.flow.asFlow
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.toList
import okhttp3.*
import org.jsoup.Jsoup

class BH3Repository(private val httpClient: OkHttpClient) : IComicsRepository {
    override suspend fun fetchComicsList(page: Int): KResult<List<Comics>> {
        if (page == 0) {
            val document = Jsoup.connect("https://comic.bh3.com/book").get()
            val containerFlow = document.getElementsByClass("container").asFlow()
            val list = containerFlow.map { element ->

                val href = "https://comic.bh3.com/book/${element.id()}"
                val title = element.getElementsByClass("container-title").text()
                val detail = element.getElementsByClass("container-description").text()
                val cover = element.getElementsByTag("img").attr("src")
                Comics(
                    comicsId = element.id(),
                    href = href,
                    name = title,
                    detail = detail,
                    cover = cover
                )
            }.toList()
            return KResult.Success(list)
        } else {
            return KResult.Success(emptyList())
        }
    }


    override suspend fun fetchComicsDetail(comics: Comics): KResult<ComicsDetail> {
        val list = loadMoreChapters(comics.href)
        return KResult.Success(ComicsDetail(list[list.lastIndex].updateTime, list))
    }

    private fun loadMoreChapters(href: String): List<ComicsChapter> {
        val request = Request.Builder()
            .url("$href/get_chapter")
            .get()
            .build()
        val response = httpClient.newCall(request).execute().body?.string()
        return response?.let { it ->
            val list = Gson().fromJson<List<ComicsChapterJson>>(
                it,
                object : TypeToken<List<ComicsChapterJson>>() {}.type
            )
            list.map {
                ComicsChapter(
                    chapterId = it.chapterId,
                    href = "https://comic.bh3.com/book/${it.bookId}/${it.chapterId}",
                    cover =
                    "https://comicstatic.bh3.com/new_static_v2/comic/chapter_cover/${it.bookId}/${it.chapterId}.jpg",
                    name = it.title,
                    pagesCount = it.pageCount,
                    updateTime = it.updateTime,
                )
            }
        } ?: emptyList()

    }

    override suspend fun fetchComicsBookList(
        comics: Comics,
        chapter: ComicsChapter
    ): KResult<ComicsBook> {
        val list = (1 until chapter.pagesCount).map {
            val imgIndex = String.format("%04d", it)
            val url =
                "https://comicstatic.bh3.com/new_static_v2/comic/book/${comics.comicsId}/${chapter.chapterId}/$imgIndex.jpg"
            url
        }
        return KResult.Success(ComicsBook(list))
    }

    override suspend fun fetchFilterList(): KResult<List<String>> {
        TODO("Not yet implemented")
    }

    override suspend fun filter(filter: String): KResult<Unit> {
        TODO("Not yet implemented")
    }

    override suspend fun search(search: String): KResult<List<Comics>> {
        TODO("Not yet implemented")
    }


}