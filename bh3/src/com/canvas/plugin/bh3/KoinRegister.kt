package com.canvas.plugin.bh3

import com.canvas.plugin.core.IKoinRegister
import org.koin.core.context.loadKoinModules
import org.koin.core.context.unloadKoinModules

class KoinRegister : IKoinRegister {
    override fun register() {
        loadKoinModules(bh3Module)
    }

    override fun unregister() {
        unloadKoinModules(bh3Module)
    }
}