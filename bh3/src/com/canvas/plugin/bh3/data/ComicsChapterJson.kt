package com.canvas.plugin.bh3.data

import com.google.gson.annotations.SerializedName


data class ComicsChapterJson(
    @SerializedName("bookid")
    val bookId: String,
    @SerializedName("chapterid")
    val chapterId: String,
    @SerializedName("title")
    val title: String,
    @SerializedName("page")
    val pageCount: Int,
    @SerializedName("timestamp")
    val updateTime: String
)