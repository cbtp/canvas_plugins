package com.canvas.plugin.bh3

import com.canvas.plugin.data.repository.IComicsRepository
import org.koin.core.qualifier.named
import org.koin.dsl.module

val bh3Module = module {
    single<IComicsRepository>(named("BH3"),override = true) {
        BH3Repository(get())
    }
}